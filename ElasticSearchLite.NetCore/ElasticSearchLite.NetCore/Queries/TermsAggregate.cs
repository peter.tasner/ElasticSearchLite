﻿using ElasticSearchLite.NetCore.Interfaces;
using ElasticSearchLite.NetCore.Interfaces.TermsAggregate;
using ElasticSearchLite.NetCore.Models;
using ElasticSearchLite.NetCore.Models.Conditions;
using ElasticSearchLite.NetCore.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ElasticSearchLite.NetCore.Queries
{
    public class TermsAggregate : AbstractConditionalQuery
    {
        internal List<ElasticField> TermAggregations { get; } = new List<ElasticField>();
        internal int Size { get; set; }

        protected TermsAggregate(IElasticPoco poco) : base(poco) { }

        protected TermsAggregate(string indexName) : base(indexName) { }

        public static IFilterableTermsAggregatedQuery<TPoco> In<TPoco>(string indexName = "")
            where TPoco : IElasticPoco
        {
            indexName = string.IsNullOrWhiteSpace(indexName) ? $"{typeof(TPoco).Name.ToLower()}index" : indexName;

            return new TermsAggregate<TPoco>(indexName);
        }
    }

    internal class TermsAggregate<TPoco> :
        TermsAggregate,
        IFilterableTermsAggregatedQuery<TPoco>,
        IFilteredTermsAggregatedQuery<TPoco>,
        IExecutableTermsAggregatedQuery<TPoco>
        where TPoco : IElasticPoco
    {
        internal TermsAggregate(string indexName) : base(indexName)
        {
        }
        /// <summary>
        /// Term Query finds documents that contain the exact term specified in the inverted index. Doesn't affect the score.
        /// </summary>
        /// <param name="propertyExpression">Field name</param>
        /// <param name="value">Value which should equal the field content</param>
        /// <returns></returns>
        public IFilteredTermsAggregatedQuery<TPoco> Term(Expression<Func<TPoco, object>> propertyExpression, object value)
        {
            TermCondition = new ElasticTermCodition()
            {
                Field = new ElasticField { Name = GetCorrectPropertyName(propertyExpression) },
                Values = new List<object> { value ?? throw new ArgumentNullException(nameof(value)) }
            };

            return this;
        }
        /// <summary>
        /// he most simple query, which matches all documents, giving them all a _score of 1.0.
        /// </summary>
        /// <returns></returns>
        public IFilteredTermsAggregatedQuery<TPoco> MatchAll()
        {
            return this;
        }
        /// <summary>
        /// Match Query for full text search.
        /// </summary>
        /// <param name="propertyExpression">Field property</param>
        /// <param name="value">Value matching the field</param>
        /// <returns></returns>
        public IFilteredTermsAggregatedQuery<TPoco> Match(Expression<Func<TPoco, object>> propertyExpression, object value)
        {
            MatchCondition = new ElasticMatchCodition
            {
                Field = new ElasticField { Name = GetCorrectPropertyName(propertyExpression) },
                Value = value ?? throw new ArgumentNullException(nameof(value)),
                Fuzziness = 0
            };

            return this;
        }
        /// <summary>
        /// match_phrase Query for search whole phrases.
        /// </summary>
        /// <param name="propertyExpression">Field property</param>
        /// <param name="value">Value matching the field</param>
        /// <returns></returns>
        public IFilteredTermsAggregatedQuery<TPoco> MatchPhrase(Expression<Func<TPoco, object>> propertyExpression, string value)
        {
            MatchPhraseCondition = new ElasticMatchPhraseCondition
            {
                Field = new ElasticField { Name = GetCorrectPropertyName(propertyExpression) },
                Value = value ?? throw new ArgumentNullException(nameof(value)),
                Slop = 0
            };

            return this;
        }
        /// <summary>
        /// match_phrase_prefix Query for auto_complete like functionality.
        /// </summary>
        /// <param name="propertyExpression">Field property</param>
        /// <param name="value">Value matching the field</param>
        /// <returns></returns>
        public IFilteredTermsAggregatedQuery<TPoco> MatchPhrasePrefix(Expression<Func<TPoco, object>> propertyExpression, string value)
        {
            MatchPhrasePrefixCondition = new ElasticMatchPhrasePrefixCondition
            {
                Field = new ElasticField { Name = GetCorrectPropertyName(propertyExpression) },
                Value = value ?? throw new ArgumentNullException(nameof(value))
            };

            return this;
        }
        /// <summary>
        /// Returns document for a numeric range or timeinterval. It doesn't affect the score (filter context).
        /// </summary>
        /// <param name="propertyExpression">Field property</param>
        /// <param name="op">Range operator</param>
        /// <param name="value"></param>
        /// <returns></returns>
        public IFilteredTermsAggregatedQuery<TPoco> Range(Expression<Func<TPoco, object>> propertyExpression, ElasticRangeOperations rangeOperation, object value)
        {
            var condition = new ElasticRangeCondition
            {
                Field = new ElasticField { Name = GetCorrectPropertyName(propertyExpression) },
                Operation = rangeOperation ?? throw new ArgumentNullException(nameof(rangeOperation)),
                Value = value ?? throw new ArgumentNullException(nameof(value))
            };
            RangeConditions.Add(condition);

            return this;
        }
        /// <summary>
        /// A multi-bucket value source based aggregation where buckets are dynamically built - one per unique value.
        /// </summary>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        public IFilteredTermsAggregatedQuery<TPoco> Terms(Expression<Func<TPoco, object>> propertyExpression)
        {
            TermAggregations.Add(new ElasticField { Name = GetCorrectPropertyName(propertyExpression) });

            return this;
        }
        /// <summary>
        /// The size parameter can be set to define how many term buckets should be returned out of the overall terms list. 
        /// By default, the node coordinating the search process will request each shard to provide its own top size term buckets and once all shards respond, 
        /// it will reduce the results to the final list that will then be returned to the client. This means that if the number of unique terms is greater than size, 
        /// the returned list is slightly off and not accurate 
        /// (it could be that the term counts are slightly off and it could even be that a term that should have been in the top size buckets was not returned).
        /// </summary>
        /// <returns></returns>
        public IExecutableTermsAggregatedQuery<TPoco> SetSize(int size)
        {
            Size = size;

            return this;
        }
        /// <summary>
        /// The size parameter can be set to define how many term buckets should be returned out of the overall terms list. 
        /// By default, the node coordinating the search process will request each shard to provide its own top size term buckets and once all shards respond, 
        /// it will reduce the results to the final list that will then be returned to the client. This means that if the number of unique terms is greater than size, 
        /// the returned list is slightly off and not accurate 
        /// (it could be that the term counts are slightly off and it could even be that a term that should have been in the top size buckets was not returned).
        /// </summary>
        /// <returns></returns>
        public IExecutableTermsAggregatedQuery<TPoco> WithoutSizeLimit()
        {
            Size = 0;

            return this;
        }
    }
}
