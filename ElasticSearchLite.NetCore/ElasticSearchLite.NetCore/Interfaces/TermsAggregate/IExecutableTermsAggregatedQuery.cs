﻿namespace ElasticSearchLite.NetCore.Interfaces.TermsAggregate
{
    public interface IExecutableTermsAggregatedQuery<TPoco> : IQuery
        where TPoco : IElasticPoco
    {
    }
}
