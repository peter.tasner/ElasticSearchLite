﻿using System.Collections.Generic;

namespace ElasticSearchLite.NetCore.Models
{
    public class ElasticTermsAggregationResponse
    {
        public List<TermsAggregations> Buckets { get; set; }
    }

    public class TermsAggregations
    {
        public string Key { get; set; }
        public long Count { get; set; }
        public List<TermsAggregations> Buckets { get; set; }
    }
}
