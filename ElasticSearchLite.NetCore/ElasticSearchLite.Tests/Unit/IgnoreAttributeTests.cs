﻿using ElasticSearchLite.NetCore.Queries;
using ElasticSearchLite.Tests.Pocos;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ElasticSearchLite.Tests.Unit
{
    [TestClass]
    public class IgnoreAttributeTests : AbstractQueryTest
    {
        private readonly PocoForIgnore testPoco = new PocoForIgnore
        {
            Id = "id-1337",
            Index = $"{nameof(PocoForIgnore).ToLower()}index",
            Type = nameof(PocoForIgnore).ToLower(),
            ShouldBeIgnored = "I won't be serialized",
            ShouldBeIndexed = "I'll be serialized"
        };

        [TestMethod]
        public void Skip_All_Properties_With_IgnoreAttribute()
        { 
            var indexCall = Index.Document<PocoForIgnore>(testPoco);

            this.TestQueryString(@"{""shouldBeIndexed"":""I'll be serialized""}", indexCall);
        }
    }
}
