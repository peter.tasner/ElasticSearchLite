﻿using ElasticSearchLite.NetCore.Queries;
using ElasticSearchLite.Tests.Pocos;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ElasticSearchLite.Tests.Unit
{
    [TestClass]
    public class TermAggregationTests : AbstractQueryTest
    {
        [TestMethod]
        public void TermsAggregation_Single_Term_Test()
        {
            var query = TermsAggregate.In<Poco>()
                .MatchAll()
                .Terms(p => p.TestInteger)
                .WithoutSizeLimit();

            var statementObject = new
            {
                size = 0,
                aggs = new
                {
                    group_by = new
                    {
                        terms = new
                        {
                            field = "testInteger"
                        }
                    }
                }
            };

            TestQueryObject(statementObject, query, true);
        }

        [TestMethod]
        public void TermsAggregation_Multiple_Term_Test()
        {
            var query = TermsAggregate.In<Poco>()
                .MatchAll()
                .Terms(p => p.TestInteger)
                .Terms(p => p.TestText)
                .WithoutSizeLimit();

            var statementObject = new
            {
                size = 0,
                aggs = new
                {
                    group_by = new
                    {
                        terms = new
                        {
                            field = "testInteger"
                        },
                        aggs = new
                        {
                            group_by = new
                            {
                                terms = new
                                {
                                    field = "testText"
                                }
                            }
                        }
                    }
                }
            };

            TestQueryObject(statementObject, query, true);
        }
    }
}
