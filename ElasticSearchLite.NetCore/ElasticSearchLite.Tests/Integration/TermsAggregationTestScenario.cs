﻿using ElasticSearchLite.NetCore;
using ElasticSearchLite.NetCore.Queries;
using ElasticSearchLite.Tests.Pocos;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading;

namespace ElasticSearchLite.Tests.Integration
{
    [TestClass]
    public class TermsAggregationTestScenario : AbstractIntegrationScenario
    {
        private readonly Random rnd = new Random();
        private List<Poco> pocos = new List<Poco>();

        [TestInitialize]
        public void Init()
        {
            for (int i = 0; i < 100; i++)
            {
                pocos.Add(new Poco
                {
                    Id = $"{i}",
                    Index = "textindex",
                    Type = "text",
                    TestText = $"{Math.Pow(i, 2.0f)}",
                    TestInteger = i,
                    TestDouble = rnd.NextDouble(),
                    TestDateTime = new DateTime(2017, 1, 1).AddDays(i)
                });
            }

            pocos.ForEach(poco => Index.Document(poco).ExecuteWith(_client));

            Thread.Sleep(1000);
        }

        [TestMethod]
        public void Terms_Aggregation_Scenario()
        {
            var response = TermsAggregate.In<Poco>("textindex")
                .MatchAll()
                .Terms(p => p.TestInteger)
                .WithoutSizeLimit()
                .ExecuteWith(_client);

            response.Buckets.Count.Should().BeGreaterThan(0);
        }
    }
}
