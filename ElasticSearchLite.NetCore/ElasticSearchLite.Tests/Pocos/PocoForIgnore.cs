﻿using ElasticSearchLite.NetCore.Attributes;
using ElasticSearchLite.NetCore.Interfaces;

namespace ElasticSearchLite.Tests.Pocos
{
    public class PocoForIgnore : IElasticPoco
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Index { get; set; }
        public double? Score { get; set; }
        public long Total { get; set; }
        public long Version { get; set; }

        public string ShouldBeIndexed { get; set; }

        [Ignore]
        public string ShouldBeIgnored { get; set; }
    }
}
